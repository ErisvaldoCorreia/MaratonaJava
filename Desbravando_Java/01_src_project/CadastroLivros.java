public class CadastroLivros {

  public static void main(String[] args) {

    // Instanciando um objeto tipo Livro
    Livros livro1 = new Livros();

    // Populando dados na instancia criada
    livro1.nome = "Desbravando Java";
    livro1.valor = 59.90;
    livro1.descricao = "Aprendendo Java com Orientação a Objetos";
    livro1.isbn = "000.00.00000.00.0";

    // Exibindo os dados via método da classe
    livro1.retornaDados();

  }

}
