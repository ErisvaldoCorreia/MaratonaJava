public class Livros {

  // Declaração dos atributos gerais
  String nome;
  String descricao;
  double valor;
  String isbn;

  void retornaDados() {

    System.out.println(
                  "Nome do Livro: " + nome +
                  "\nDescrição: "  + descricao +
                  "\nValor: " + valor +
                  "\nISBN: " + isbn );
  }

}
