public class CadastroLivros {

  public static void main(String[] args) {

    // criando as instâncias de referências
    Livros livro = new Livros();
    Autor autor = new Autor();

    // populando os dados
    livro.nome = "Desbravando Java";
    livro.valor = 59.90;
    livro.descricao = "Aprendendo Java";
    livro.isbn = "000.00.00000.00.0";

    // referenciando os dados do autor na classe livro.
    livro.autor = autor;

    autor.nome = "Fulano de Tal";
    autor.cpf = "000.000.000-00";
    autor.email = "fulano@email.com";

    // chamando o método retorna dados da classe livro.
    livro.retornaDados();
  }

}
