public class Livros {

  // Declaração dos atributos gerais
  String nome;
  String descricao;
  double valor;
  String isbn;

  // Criando referência ao autor do livro
  Autor autor;

  // Método que retorna os dados do livro!
  void retornaDados() {
    System.out.println(
                  "Nome do Livro: " + nome +
                  "\nDescrição: "  + descricao +
                  "\nValor: " + valor +
                  "\nISBN: " + isbn +
                  "\n------");

    // Chamada do método de exibir detalhes do autor em sua classe
    autor.mostraDetalhesAutor();
  }

}
