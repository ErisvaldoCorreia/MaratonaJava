public class Autor {

  // Atributos da classe Autor
  String nome;
  String cpf;
  String email;

  // Método para retornar os dados do autor
  // Será invocado primordialmente pela classe livro.
  void mostraDetalhesAutor() {
    System.out.println(
                  "Nome do Autor: " + nome +
                  "\nEmail: "  + email +
                  "\nCPF: " + cpf);
  }

}
