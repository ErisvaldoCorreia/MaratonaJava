/**
 * Aplicando conceitos de métodos com e sem retorno
 * testando retornos em chamadas de métodos.
 */

public class Livros {

  // Declaração dos atributos gerais
  String nome;
  String descricao;
  double valor;
  String isbn;

  // Criando referência ao autor do livro
  Autor autor;

  /**
   * Utilizando o this para refenrenciar atributos e métodos
   */

  // Método que retorna os dados do livro!
  void retornaDados() {
    System.out.println(
                  "Nome do Livro: " + this.nome +
                  "\nDescrição: "  + this.descricao +
                  "\nValor: " + this.valor +
                  "\nISBN: " + this.isbn +
                  "\n------");

    /**
     * Verifica se o retorno do método temAutor é verdadeiro.
     * Caso seja True, realiza a chamada ao método da classe Autor
     */
    if (this.temAutor()){
      autor.mostraDetalhesAutor();
    }

  }

  // Método que aplica desconto em uma determinada porcentagem
  void colocaDescontoDe(double porcentagem) {
    this.valor -= this.valor * porcentagem;
  }

  // Método que verifica se existe algum autor cadastrado junto ao livro
  boolean temAutor() {
    return this.autor != null;
  }

}
