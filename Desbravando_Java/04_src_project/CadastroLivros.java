public class CadastroLivros {

  public static void main(String[] args) {

    // criando as instâncias de referências
    Livros livro = new Livros();
    Autor autor = new Autor();

    // populando os dados
    livro.nome = "Desbravando Java";
    livro.descricao = "Aprendendo Java";
    livro.isbn = "000.00.00000.00.0";

    // usando a função de manipular o valor para ter acesso ao atributo privado
    livro.adicionaValor(59.90);

    // referenciando os dados do autor na classe livro.
    livro.autor = autor;

    // populando dados para o autor
    autor.nome = "Fulano de Tal";
    autor.cpf = "000.000.000-00";
    autor.email = "fulano@email.com";

    // chamando o método retorna dados da classe livro.
    livro.retornaDados();
  }

}
