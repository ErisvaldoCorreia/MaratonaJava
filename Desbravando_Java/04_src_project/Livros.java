/**
 * Aplicando conceitos de Encapsulamento
 * Principios iniciais!
 */

public class Livros {

  // Declaração dos atributos gerais
  String nome;
  String descricao;
  String isbn;

  // tornando o valor com acesso restrito
  private double valor;

  // Criando referência ao autor do livro
  Autor autor;

  /**
   * Utilizando a função de acesso ao valor para imprimirmos seu preço!
   */

  // Método que retorna os dados do livro!
  void retornaDados() {
    System.out.println(
                  "Nome do Livro: " + this.nome +
                  "\nDescrição: "  + this.descricao +
                  "\nValor: " + this.retornaValor() +
                  "\nISBN: " + this.isbn +
                  "\n------");

    /**
     * Verifica se o retorno do método temAutor é verdadeiro.
     * Caso seja True, realiza a chamada ao método da classe Autor
     */
    if (this.temAutor()){
      autor.mostraDetalhesAutor();
    }

  }

  // Método que verifica se existe algum autor cadastrado junto ao livro
  boolean temAutor() {
    return this.autor != null;
  }

  // Método para acessar a inclusão de valores no valor!
  void adicionaValor(double valor) {
    this.valor = valor;
  }

  // Método para retornar o valor do livro.
  double retornaValor() {
    return this.valor;
  }

}
