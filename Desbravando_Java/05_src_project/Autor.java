public class Autor {

  // Atributos da classe Autor
  private String nome;
  private String cpf;
  private String email;

  /**
   * Métodos getters e setters do autor
   */

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNome() {
    return this.nome;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getCpf() {
    return this.cpf;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return this.email;
  }

  /**
   * Método para retornar os dados do autor
   * Será invocado primordialmente pela classe livro.
   */
  void mostraDetalhesAutor() {
    System.out.println(
                  "Nome do Autor: " + this.getNome() +
                  "\nEmail: "  + this.getEmail() +
                  "\nCPF: " + this.getCpf() +
                  "\n---------------------");
  }

}
