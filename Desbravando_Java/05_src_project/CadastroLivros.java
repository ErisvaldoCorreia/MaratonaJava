public class CadastroLivros {

  public static void main(String[] args) {

    // criando as instâncias de referências
    Livros livro = new Livros();
    Autor autor = new Autor();

    // populando os dados
    livro.setNome("Desbravando Java");
    livro.setDescricao("Aprendendo Java");
    livro.setIsbn("000.00.00000.00.0");
    livro.setValor(59.90);

    // referenciando os dados do autor na classe livro.
    livro.setAutor(autor);

    // populando dados para o autor
    autor.setNome("Fulano de Tal");
    autor.setCpf("000.000.000-00");
    autor.setEmail("fulano@email.com");

    // chamando o método retorna dados da classe livro.
    livro.retornaDados();
  }

}
