/**
 * Encapsulamento
 * Getters e Setters
 */

public class Livros {

  // Declaração dos atributos gerais privados
  private String nome;
  private String descricao;
  private String isbn;
  private double valor;

  // Criando referência ao autor do livro (privado)
  private Autor autor;

  /**
   * Utilizando os getters e setters para manipular os dados
   */

   public void setValor(double valor) {
     this.valor = valor;
   }

   public double getValor() {
     return this.valor;
   }

   public void setNome(String nome) {
     this.nome = nome;
   }

   public String getNome() {
     return this.nome;
   }

   public void setDescricao(String descricao) {
     this.descricao = descricao;
   }

   public String getDescricao() {
     return this.descricao;
   }

   public void setIsbn(String isbn) {
     this.isbn = isbn;
   }

   public String getIsbn() {
     return this.isbn;
   }

   public void setAutor(Autor autor) {
     this.autor = autor;
   }

   public Autor getAutor() {
     return this.autor;
   }

  // Método que retorna os dados do livro usando getters
  void retornaDados() {
    System.out.println(
                  "Nome do Livro: " + this.getNome() +
                  "\nDescrição: "  + this.getDescricao() +
                  "\nValor: " + this.getValor() +
                  "\nISBN: " + this.getIsbn() +
                  "\n------");

    /**
     * Verifica se o retorno do método temAutor é verdadeiro.
     * Caso seja True, realiza a chamada ao método da classe Autor
     */
    if (this.temAutor()){
      autor.mostraDetalhesAutor();
    }

  }

  // Método que verifica se existe algum autor cadastrado junto ao livro
  boolean temAutor() {
    return this.getAutor() != null;
  }

}
