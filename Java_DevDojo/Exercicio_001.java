/**
 * @author: Erivaldo Correia
 * Estudos Maratona Java Dev Dojo
 *
 */

class Exercicio_001 {

  public static void main(String[] args) {

    // imprimindo uma mensagem
    System.out.println("Olá mundo!");

    // imprimindo uma mensagem em duas linhas
    System.out.println("Quebrando a frase\nSegunda Linha");

  }
}
