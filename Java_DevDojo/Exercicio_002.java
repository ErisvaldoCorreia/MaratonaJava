/**
 * @author: Erivaldo Correia
 * Estudos Maratona Java Dev Dojo
 *
 */

class Exercicio_002 {

  public static void main(String[] args) {

    // declarando uma variavel com inicialização
    int idade = 32;

    // declarando uma variavel sem inicializar
    int anoNascimento;

    // recebendo o valor numa variavel ja declarada
    anoNascimento = 1987;

    /*
     * exibindo uma mensagem com os valores da variavel.
     * usamos o operador + para concatenar a mensagem entre aspas
     * e o valor guardado na variavel.
     *
     */
    System.out.println("Idade = " + idade + "\nAno de Nascimento = " + anoNascimento);

  }
}
