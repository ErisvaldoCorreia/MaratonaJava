/**
 * @author: Erivaldo Correia
 * Estudos Maratona Java Dev Dojo
 *
 */

// as classes seguem a regra CamelCase em sua nomeclatura
class Exercicio_003 {

  // método principal do programa
  public static void main(String[] args) {

    // quebra de linha para evitar codigos muito longos
    System.out.println(
              "Esta mensagem ficaria muito longa em uma linha só\n" +
              "por isso ela foi quebrada para facilitar a leitura");
    // usamos uma identação para ajustar a linha de código

  }
}
