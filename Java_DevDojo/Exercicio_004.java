/**
 * @author: Erivaldo Correia
 * Estudos Maratona Java Dev Dojo
 *
 */

public class Exercicio_004 {

  public static void main(String[] args) {

    // Trabalhando com variaveis não inicializadas
    int idade;

    // inicializando uma variavel.
    idade = 31;

    // Trabalhando com variaiveis com inicialização
    String nome = "Erisvaldo Correia";


    System.out.println(
                  "Meu nome é " + nome +
                  " e tenho " + idade +
                  " anos!");

    // Imprimindo as variaveis!
  }
}
